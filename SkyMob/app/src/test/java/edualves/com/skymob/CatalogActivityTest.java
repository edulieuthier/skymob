package edualves.com.skymob;

import android.widget.TextView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import edualves.com.skymob.ui.CatalogActivity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by edualves on 15/06/17.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class CatalogActivityTest {

    CatalogActivity activity;

    @Before
    public void setUp() throws Exception {
        activity = Robolectric.setupActivity(CatalogActivity.class);
    }

    @Test
    public void catalogFragmentShouldNotBeNull() {
        TextView textTitle = (TextView) activity.findViewById(R.id.title_screen);

        assertNotEquals("Fantásticos filmes", textTitle.getText().toString());
        assertEquals("Uma seleção de filmes imperdíveis", textTitle.getText().toString());
    }

}
