package edualves.com.skymob;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import edualves.com.skymob.model.MovieResponse;
import edualves.com.skymob.network.NetworkService;
import edualves.com.skymob.network.Service;
import edualves.com.skymob.presenter.MoviesPresenter;
import edualves.com.skymob.ui.CatalogActivity;
import edualves.com.skymob.ui.CatalogView;
import edualves.com.skymob.utils.Utils;
import retrofit2.Retrofit;
import rx.Observable;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by edualves on 15/06/17.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class CatalogBehaviorTests {

    Gson gson = new GsonBuilder().create();

    MoviesPresenter moviesPresenter;

    @Mock
    Service service;

    @Mock
    NetworkService networkService;

    @Mock
    CatalogView view;

    Retrofit retrofit = new Retrofit.Builder().baseUrl("http://localhost/").build();

    @Rule
    public ActivityTestRule<CatalogActivity> activityTestRule = new ActivityTestRule<>(CatalogActivity.class);


    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);

        moviesPresenter = new MoviesPresenter(service, view);
    }

    @Test
    public void checkTitleScreenTest() {
        onView(withId(R.id.title_screen)).check(matches(withText("Uma seleção de filmes imperdíveis")));
    }

    @Test
    public void scrollOnScreenTest() throws InterruptedException, IOException {

        Type type = new TypeToken<ArrayList<MovieResponse>>() {
        }.getType();

        List<MovieResponse> movieResponse = gson.fromJson(Utils.readJson(InstrumentationRegistry.getContext()), type);

        List<MovieResponse> spyResponse = Mockito.spy(movieResponse);

        Log.d("TEST", "Movie 0: " + movieResponse.get(0));

        Mockito.when(service.getMovies()).thenReturn(Observable.just(spyResponse));

        moviesPresenter.getMoviesList();

        onView(withId(R.id.recycler_list))
                .perform(RecyclerViewActions.actionOnItemAtPosition(6, click()));

    }

}
