package edualves.com.skymob.network;

import java.util.List;

import edualves.com.skymob.model.MovieResponse;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by edualves on 14/06/17.
 */

public interface NetworkService {

    @GET("Movies")
    Observable<List<MovieResponse>> getResults();
}
