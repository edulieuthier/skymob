package edualves.com.skymob.ui;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import edualves.com.skymob.BaseApp;
import edualves.com.skymob.R;
import edualves.com.skymob.model.MovieResponse;
import edualves.com.skymob.network.Service;
import edualves.com.skymob.presenter.MoviesPresenter;

public class CatalogActivity extends BaseApp implements CatalogView {

    RecyclerView recyclerList;

    MoviesPresenter presenter;

    @Inject
    public Service service;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog);

        getDeps().inject(this);
        init();

        presenter = new MoviesPresenter(service, this);

        presenter.getMoviesList();
    }

    private void init() {
        recyclerList = (RecyclerView) findViewById(R.id.recycler_list);
        recyclerList.setLayoutManager(new GridLayoutManager(this, 2));
    }

    @Override
    public void getListTvShowsSuccess(final List<MovieResponse> movieResponseList) {
        MovieAdapter adapter = new MovieAdapter(
                this,
                movieResponseList,
                new MovieAdapter.OnItemClickListener() {
                    @Override
                    public void onClick(MovieResponse resultItem) {
                        Toast.makeText(
                                CatalogActivity.this,
                                resultItem.getTitle(),
                                Toast.LENGTH_LONG).show();
                    }
                });

        recyclerList.setAdapter(adapter);
    }

    @Override
    public void failureListMovies(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }
}
