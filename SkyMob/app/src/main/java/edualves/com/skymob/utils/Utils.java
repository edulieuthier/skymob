package edualves.com.skymob.utils;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by edualves on 16/06/17.
 */

public class Utils {

    public static String readJson(Context context) {

        String json;

        try {
            InputStream inputStream = context.getAssets().open("sky_movies.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];
            inputStream.read(buffer);
            inputStream.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return json;
    }
}
