package edualves.com.skymob.presenter;

import android.accounts.NetworkErrorException;
import android.util.Log;

import java.util.List;

import edualves.com.skymob.model.MovieResponse;
import edualves.com.skymob.network.Service;
import edualves.com.skymob.ui.CatalogView;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by edualves on 14/06/17.
 */

public class MoviesPresenter {

    private final Service service;

    private final CatalogView view;

    private CompositeSubscription subscriptions;

    private final String LOG_TAG = MoviesPresenter.class.getName();

    public MoviesPresenter(Service service, CatalogView view) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeSubscription();

    }

    public void getMoviesList() {

        Observable subscription = service.getMovies();

        subscription.subscribe(new Subscriber<List<MovieResponse>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.d(LOG_TAG, "OnError:"+e.getMessage());
                view.failureListMovies(e.getMessage());
            }

            @Override
            public void onNext(List<MovieResponse> movieResponsesList) {
                Log.d(LOG_TAG, "OnSuccess:OK");
                Log.d(LOG_TAG, "OnSuccess:"+ movieResponsesList);
                view.getListTvShowsSuccess(movieResponsesList);

            }
        });

    }

    public void onStop() {
        subscriptions.unsubscribe();
    }
}
