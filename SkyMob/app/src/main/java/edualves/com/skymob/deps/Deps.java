package edualves.com.skymob.deps;

import javax.inject.Singleton;

import dagger.Component;
import edualves.com.skymob.network.NetworkModule;
import edualves.com.skymob.ui.CatalogActivity;

/**
 * Created by edualves on 14/06/17.
 */

@Singleton
@Component(modules = NetworkModule.class)
public interface Deps {

    void inject(CatalogActivity catalogActivity);
}
