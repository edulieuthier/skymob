package edualves.com.skymob.ui;

import java.util.List;

import edualves.com.skymob.model.MovieResponse;

/**
 * Created by edualves on 14/06/17.
 */

public interface CatalogView {

    void getListTvShowsSuccess(List<MovieResponse> movieResponseList);

    void failureListMovies(String errorMessage);

}
