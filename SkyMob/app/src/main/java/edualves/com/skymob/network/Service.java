package edualves.com.skymob.network;

import android.accounts.NetworkErrorException;
import android.util.Log;

import java.util.List;

import edualves.com.skymob.model.MovieResponse;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by edualves on 14/06/17.
 */

public class Service {

    private final NetworkService service;

    public Service(NetworkService service) {
        this.service = service;
    }

    public Observable getMovies() {

        return service.getResults()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends List<MovieResponse>>>() {
                    @Override
                    public Observable<? extends List<MovieResponse>> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                });
    }


    public interface GetResultsCallback {

        void onSuccess(List<MovieResponse> movieResponseList);

        void onError(NetworkErrorException exception);
    }
}
