package edualves.com.skymob;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import edualves.com.skymob.deps.DaggerDeps;
import edualves.com.skymob.deps.Deps;
import edualves.com.skymob.network.NetworkModule;

/**
 * Created by edualves on 14/06/17.
 */

public class BaseApp extends AppCompatActivity {

    Deps deps;

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);

        deps = DaggerDeps.builder().networkModule(new NetworkModule()).build();
    }

    public Deps getDeps() {
        return deps;
    }
}
